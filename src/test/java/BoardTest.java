import static org.junit.jupiter.api.Assertions.*;

import com.sf9000.worldcup.scoreboard.Board;
import com.sf9000.worldcup.scoreboard.Game;
import com.sf9000.worldcup.scoreboard.GameAlreadyStartedException;
import com.sf9000.worldcup.scoreboard.GameNotFoundException;
import com.sf9000.worldcup.scoreboard.TeamNotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class BoardTest {

  @DisplayName("Start Game")
  @Test
  void startGameTest() throws Exception {

    Board board = new Board();
    board.startGame("Mexico","Canada");

    List<Game> gameList = board.getSummary();

    assertNotNull(gameList);
    assertEquals(1, gameList.size());

  }

  @DisplayName("Initial Score is 0-0")
  @Test
  void initialScoreIsZeroZeroTest() throws Exception {

    Board board = new Board();
    board.startGame("Mexico","Canada");

    List<Game> gameList = board.getSummary();

    Game game = gameList.get(0);

    String score = game.getScore();

    assertEquals("0 - 0", score);

  }

  @DisplayName("Finish Game")
  @Test
  void finishGameTest() throws Exception {

    Board board = new Board();
    board.startGame("Mexico","Canada");

    board.finishGame("Mexico","Canada");

    List<Game> gameList = board.getSummary();

    assertNotNull(gameList);
    assertEquals(0, gameList.size());

  }

  @DisplayName("Update Game Score")
  @Test
  void updateScoreTest() throws Exception {

    Board board = new Board();
    board.startGame("Mexico","Canada");

    board.updateScore("Mexico",2,"Canada",1);

    List<Game> gameList = board.getSummary();

    Game game = gameList.get(0);

    String score = game.getScore();

    assertEquals("2 - 1", score);

    assertEquals(1, board.getSummary().size(), "It is possible to have only one game with same teams.");

  }

  @DisplayName("Get a summary of games by total score")
  @Test
  void getSummaryOfGamesByTotalScore() throws Exception {

    Board board = createBoardWithFiveGames();

    List<Game> expectedList = createExpectedListOfGames();

    assertEquals(expectedList, board.getSummary());

  }

  @DisplayName("Error when adding repeated game")
  @Test
  void throwsErrorWhenAddingRepeatedGame() {

    Board board = new Board();
    try {
      board.startGame("Mexico", "Canada");
    } catch (GameAlreadyStartedException e) {
      e.printStackTrace();
    }

    assertThrows(GameAlreadyStartedException.class, () -> board.startGame("Mexico", "Canada"));

  }

  @DisplayName("Error when updating an invalid game")
  @Test
  void throwsErrorWhenUpdatingScoreOfGameThatDoesNotExistOnTheBoard() {

    Board board = new Board();
    try {
      board.startGame("Mexico", "Canada");
    } catch (GameAlreadyStartedException e) {
      assertThrows(GameNotFoundException.class, () -> board.updateScore("Brazil", 1, "Spain", 1 ));
    }

  }

  @DisplayName("Error when finishing an invalid game")
  @Test
  void throwsErrorWhenFinishingAGameThatDoesNotExistOnTheBoard() {
    Board board = new Board();
    try {
      board.startGame("Mexico", "Canada");
    } catch (GameAlreadyStartedException e) {
      e.printStackTrace();
    }

    assertThrows(Exception.class, () -> board.finishGame("Brazil", "Spain"));
  }

  @DisplayName("Giving a team get its score")
  @Test
  void givenATeamGetItsScore() throws GameAlreadyStartedException, GameNotFoundException, TeamNotFoundException {
    Board board = new Board();

    board.startGame("Mexico","Canada");

    board.updateScore("Mexico",2,"Canada",1);

    int score = board.getScoreByTeam("Mexico");

    assertEquals(2, score);
  }

  @DisplayName("Giving an away team get its score")
  @Test
  void givenAnAwayTeamGetItsScore() throws GameAlreadyStartedException, GameNotFoundException, TeamNotFoundException {
    Board board = new Board();

    board.startGame("Mexico","Canada");

    board.updateScore("Mexico",2,"Canada",1);

    int score = board.getScoreByTeam("Canada");

    assertEquals(1, score);
  }

  @DisplayName("Error when getting score of invalid team")
  @Test
  void throwErrorWhenGettingScoreOfInvalidTeam() throws GameAlreadyStartedException {
    Board board = new Board();

    board.startGame("Mexico","Canada");

    assertThrows(TeamNotFoundException.class, () -> board.getScoreByTeam("Brazil"));

  }

  private List<Game> createExpectedListOfGames() {
    List<Game> expectedList = new ArrayList<>();

    expectedList.add(new Game("Uruguay","Italy"));
    expectedList.add(new Game("Spain","Brazil"));
    expectedList.add(new Game("Mexico", "Canada"));
    expectedList.add(new Game("Argentina","Australia"));
    expectedList.add(new Game("Germany","France"));
    return expectedList;
  }

  private Board createBoardWithFiveGames() throws Exception {
    Board board = new Board();
    board.startGame("Mexico", "Canada");
    board.startGame("Spain","Brazil");
    board.startGame("Germany","France");
    board.startGame("Uruguay","Italy");
    board.startGame("Argentina","Australia");

    board.updateScore("Mexico",0, "Canada", 5);
    board.updateScore("Spain",10,"Brazil",2);
    board.updateScore("Germany",2,"France",2);
    board.updateScore("Uruguay",6,"Italy",6);
    board.updateScore("Argentina",3,"Australia",1);
    return board;
  }

}
