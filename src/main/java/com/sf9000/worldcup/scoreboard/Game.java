package com.sf9000.worldcup.scoreboard;

import java.time.Instant;
import java.util.Objects;

public class Game {

  private final String homeTeam;
  private final String awayTeam;
  private int homeTeamScore;
  private int awayTeamScore;
  private final Instant createdAt;

  public Game(String homeTeam, String awayTeam) {
    this.homeTeam = homeTeam;
    this.awayTeam = awayTeam;

    createdAt = Instant.now();

  }

  public String getScore() {
    return homeTeamScore + " - " + awayTeamScore;
  }

  public int getScoreOf(String team) {
    return team.equals(homeTeam) ? homeTeamScore : awayTeamScore;
  }

  public void updateScore(int homeTeamScore, int awayTeamScore) {
    this.homeTeamScore = homeTeamScore;
    this.awayTeamScore = awayTeamScore;
  }

  public Integer getTotalScore() {
    return homeTeamScore + awayTeamScore;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }

  public String getHomeTeam() {
    return homeTeam;
  }

  public String getAwayTeam() {
    return awayTeam;
  }

  public int getHomeTeamScore() {
    return homeTeamScore;
  }

  public int getAwayTeamScore() {
    return awayTeamScore;
  }

  @Override
  public String toString() {
    return "Game{"
        + "homeTeam='" + homeTeam + '\''
        + ", awayTeam='" + awayTeam + '\''
        + ", homeTeamScore=" + homeTeamScore
        + ", awayTeamScore=" + awayTeamScore
        + ", createdAt=" + createdAt
        + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Game game = (Game) o;
    return Objects.equals(homeTeam, game.homeTeam) && Objects.equals(awayTeam, game.awayTeam);
  }

  @Override
  public int hashCode() {
    return Objects.hash(homeTeam, awayTeam);
  }

}
