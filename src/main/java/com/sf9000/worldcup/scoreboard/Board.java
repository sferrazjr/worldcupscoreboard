package com.sf9000.worldcup.scoreboard;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Board {

  List<Game> gameList = new ArrayList<>();

  public void startGame(String homeTeam, String awayTeam) throws GameAlreadyStartedException {
    Game game = new Game(homeTeam, awayTeam);

    if (gameList.contains(game)) {
      throw new GameAlreadyStartedException(
          "Game " + homeTeam + " vs " + awayTeam + " was already started.");
    }

    gameList.add(game);
  }

  public List<Game> getSummary() {

    gameList.sort(
        Comparator.comparing(Game::getTotalScore)
            .thenComparing(Game::getCreatedAt)
            .reversed());

    return gameList;
  }

  public void finishGame(String homeTeam, String awayTeam) throws GameNotFoundException {
    Game game = new Game(homeTeam, awayTeam);

    if (isGameMissingOnTheBoard(game)) {
      throw new GameNotFoundException("Game found " + homeTeam + " vs " + awayTeam + " not found.");
    }

    gameList.remove(game);
  }

  public void updateScore(String homeTeam, int homeTeamScore, String awayTeam, int awayTeamScore)
      throws GameNotFoundException {

    Game game = new Game(homeTeam, awayTeam);

    if (isGameMissingOnTheBoard(game)) {
      throw new GameNotFoundException("Game found " + homeTeam + " vs " + awayTeam + " not found.");
    }

    game.updateScore(homeTeamScore, awayTeamScore);

    int indexOfGame = gameList.indexOf(game);
    gameList.set(indexOfGame, game);

  }

  private boolean isGameMissingOnTheBoard(Game game) {
    return !gameList.contains(game);
  }

  public int getScoreByTeam(String team) throws TeamNotFoundException {

    return gameList.stream().filter(g -> g.getHomeTeam().equals(team) || g.getAwayTeam().equals(team))
        .mapToInt(g1 -> g1.getScoreOf(team)).findFirst()
        .orElseThrow(() -> new TeamNotFoundException("Team " + team + " not found on the board."));

  }
}
