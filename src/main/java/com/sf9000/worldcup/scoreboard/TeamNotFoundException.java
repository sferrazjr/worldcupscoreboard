package com.sf9000.worldcup.scoreboard;

public class TeamNotFoundException extends Exception {

  public TeamNotFoundException(String message) {
    super(message);
  }
}
