# Football World Cup Score Board

## About this challenge

### How I did
I started writing the tests for each feature from the exercise document and then implementing the actual feature.

### Decisions that I made

I decided to have only two classes, Board and Game, as the simplest possible solution.

Board has a list of games and the four methods asked in the document. As it is not clear how the customer would interact with the board, I opted to use strings as parameters, instead of a class.

Game has the teams' names and the scores that will be on the board, and the property createdAt that is necessary to order the list of games on the board.

As the exercise asks for the simplest solution for a library, I didn't create the main class as this would be out a library's scope.

### Tooling
As it is in pom.xml I used java 11 and junit5